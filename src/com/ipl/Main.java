package com.ipl;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class Main {

    public static final int ID = 0;
    public static final int SEASON = 1;
    public static final int CITY = 2;
    public static final int DATE = 3;
    public static final int TEAM1 = 4;
    public static final int TEAM2 = 5;
    public static final int TOSS_WINNER = 6;
    public static final int TOSS_DECISION = 7;
    public static final int RESULT = 8;
    public static final int DL_APPLIED = 9;
    public static final int WINNER = 10;
    public static final int WIN_BY_RUNS = 11;
    public static final int WIN_BY_WICKETS = 12;
    public static final int PLAYER_OF_MATCH = 13;
    public static final int VENUE = 14;
    public static final int UMPIRE1 = 15;
    public static final int UMPIRE2 = 16;

    public static final int MATCH_ID = 0;
    public static final int INNING = 1;
    public static final int BATTING_TEAM = 2;
    public static final int BOWLING_TEAM = 3;
    public static final int OVER = 4;
    public static final int BALL = 5;
    public static final int BATSMAN = 6;
    public static final int NON_STRIKER = 7;
    public static final int BOWLER = 8;
    public static final int IS_SUPER_OVER = 9;
    public static final int WIDE_RUNS = 10;
    public static final int BYE_RUNS = 11;
    public static final int LEG_BYE_RUNS = 12;
    public static final int NO_BALL_RUNS = 13;
    public static final int PENALTY_RUNS = 14;
    public static final int BATSMAN_RUNS = 15;
    public static final int EXTRA_RUNS = 16;
    public static final int TOTAL_RUNS = 17;
    public static final int PLAYER_DISMISSED = 18;
    public static final int DISMISSAL_KIND = 19;
    public static final int FIELDER = 20;

    public static void main(String[] args) {
        List<Match> matches = getMatchesData();
        List<Delivery> deliveries = getDeliveriesData();

        findNumberOfMatchesPlayedPerYear(matches);
        findNumberOfMatchesWonByEachTeam(matches);
        findNumberOfExtraRunsConcededByEachTeamIn2016(matches, deliveries);
        findTopEconomicalBowlersIn2015(matches, deliveries);
        findNoOfWinsOfEachTeamWhileBattingFirst(matches);

        findTopRunScoresInEverySeason(matches, deliveries);
        findHighestStrikeRateBatsmanForEachTeamIn2016(matches, deliveries);
        findHighestRunScorerAgainstSRHPerSeason(matches, deliveries);

        findMostNoOfBallsFacedBatsmanPerEachTeamInYear1015(matches, deliveries);
    }

    private static List<Match> getMatchesData() {
        List<Match> matches = new ArrayList<>();

        String matchesPath = "Data/matches.csv";
        File matchFile = new File(matchesPath);

        try {
            Scanner scanner = new Scanner(matchFile);
            if (scanner.hasNextLine()) {
                scanner.nextLine();
            }
            while (scanner.hasNextLine()) {
                String[] matchData = scanner.nextLine().split(",");
                Match match = new Match();

                match.setId(matchData[ID]);
                match.setSeason(matchData[SEASON]);
                match.setCity(matchData[CITY]);
                match.setDate(matchData[DATE]);
                match.setTeam1(matchData[TEAM1]);
                match.setTeam2(matchData[TEAM2]);
                match.setTossWinner(matchData[TOSS_WINNER]);
                match.setTossDecision(matchData[TOSS_DECISION]);
                match.setResult(matchData[RESULT]);
                match.setDlApplied(matchData[DL_APPLIED]);
                match.setWinner(matchData[WINNER]);
                match.setWinByRuns(matchData[WIN_BY_RUNS]);
                match.setWinByWickets(matchData[WIN_BY_WICKETS]);
                match.setPlayerOfMatch(matchData[PLAYER_OF_MATCH]);
                match.setVenue(matchData[VENUE]);
                if (matchData.length <= 15) {
                    continue;
                }
                match.setUmpire1(matchData[UMPIRE1]);
                match.setUmpire2(matchData[UMPIRE2]);

                matches.add(match);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return matches;
    }

    private static List<Delivery> getDeliveriesData() {
        List<Delivery> deliveries = new ArrayList<>();

        String deliveriesPath = "Data/deliveries.csv";
        File deliveriesFile = new File(deliveriesPath);

        try {
            Scanner scanner = new Scanner(deliveriesFile);
            if (scanner.hasNextLine()) {
                scanner.nextLine();
            }
            while (scanner.hasNextLine()) {
                String[] deliveryData = scanner.nextLine().split(",");
                Delivery delivery = new Delivery();
                int length = deliveryData.length;

                delivery.setMatchId(deliveryData[MATCH_ID]);
                delivery.setInning(deliveryData[INNING]);
                delivery.setBattingTeam(deliveryData[BATTING_TEAM]);
                delivery.setBowlingTeam(deliveryData[BOWLING_TEAM]);
                delivery.setOver(deliveryData[OVER]);
                delivery.setBall(deliveryData[BALL]);
                delivery.setBatsman(deliveryData[BATSMAN]);
                delivery.setNonStriker(deliveryData[NON_STRIKER]);
                delivery.setBowler(deliveryData[BOWLER]);
                delivery.setIsSuperOver(deliveryData[IS_SUPER_OVER]);
                delivery.setWideRuns(deliveryData[WIDE_RUNS]);
                delivery.setByeRuns(deliveryData[BYE_RUNS]);
                delivery.setLegByeRuns(deliveryData[LEG_BYE_RUNS]);
                delivery.setNoBallRuns(deliveryData[NO_BALL_RUNS]);
                delivery.setPenaltyRuns(deliveryData[PENALTY_RUNS]);
                delivery.setBatsmanRuns(deliveryData[BATSMAN_RUNS]);
                delivery.setExtraRuns(deliveryData[EXTRA_RUNS]);
                delivery.setTotalRuns(deliveryData[TOTAL_RUNS]);
                if (length >= 19) {
                    delivery.setPlayerDismissed(deliveryData[PLAYER_DISMISSED]);
                }
                if (length >= 20) {
                    delivery.setDismissalKind(deliveryData[DISMISSAL_KIND]);
                }
                if (length >= 21) {
                    delivery.setFielder(deliveryData[FIELDER]);
                }

                deliveries.add(delivery);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return deliveries;
    }

    public static void findNumberOfMatchesPlayedPerYear(List<Match> matches) {
        Map<Integer, Integer> matchesPlayedPerYear = new TreeMap<>();

        for (Match match : matches) {
            int season = Integer.parseInt(match.getSeason());
            matchesPlayedPerYear.put(season, matchesPlayedPerYear.getOrDefault(season, 0) + 1);
        }
        System.out.println("Number of Matches Played Per Year");
        System.out.println("*******************************************************");

        for (Map.Entry<Integer, Integer> yearAndMatches : matchesPlayedPerYear.entrySet()) {
            System.out.println("Number of matches played in the year " + yearAndMatches.getKey() + " is " + yearAndMatches.getValue());
        }
        System.out.println("*******************************************************\n");
    }

    public static void findNumberOfMatchesWonByEachTeam(List<Match> matches) {
        Map<String, Integer> matchesWonByEachTeam = new HashMap<>();

        for (Match match : matches) {
            String winnerTeam = match.getWinner();
            if (winnerTeam.equals("")) {
                continue;
            }
            matchesWonByEachTeam.put(winnerTeam, matchesWonByEachTeam.getOrDefault(winnerTeam, 0) + 1);
        }
        System.out.println("Number of Matches won by each IPL Team");
        System.out.println("*******************************************************");

        for (Map.Entry<String, Integer> teamAndWins : matchesWonByEachTeam.entrySet()) {
            System.out.println("Number of matches won by team " + teamAndWins.getKey() + " is " + teamAndWins.getValue());
        }
        System.out.println("*******************************************************\n");
    }

    public static void findNumberOfExtraRunsConcededByEachTeamIn2016(List<Match> matches, List<Delivery> deliveries) {
        Set<String> matchIdsOf2016 = new HashSet<>();

        for (Match match : matches) {
            if (match.getSeason().equals("2016")) {
                matchIdsOf2016.add(match.getId());
            }
        }
        Map<String, Integer> extraRunsConcededByEachTeam = new HashMap<>();

        for (Delivery delivery : deliveries) {
            if (!matchIdsOf2016.contains(delivery.getMatchId())) {
                continue;
            }
            String bowlingTeam = delivery.getBowlingTeam();
            extraRunsConcededByEachTeam.put(bowlingTeam, extraRunsConcededByEachTeam.getOrDefault(bowlingTeam, 0) + Integer.parseInt(delivery.getExtraRuns()));
        }
        System.out.println("Number of Extra runs conceded by each team in 2016");
        System.out.println("*******************************************************");

        for (Map.Entry<String, Integer> teamAndExtraRuns : extraRunsConcededByEachTeam.entrySet()) {
            System.out.println("Extra Runs Conceded by team " + teamAndExtraRuns.getKey() + " is " + teamAndExtraRuns.getValue());
        }
        System.out.println("*******************************************************\n");
    }

    public static void findTopEconomicalBowlersIn2015(List<Match> matches, List<Delivery> deliveries) {
        Set<String> matchIdsOf2015 = new HashSet<>();

        for (Match match : matches) {
            if (match.getSeason().equals("2015")) {
                matchIdsOf2015.add(match.getId());
            }
        }
        Map<String, List<Integer>> bowlerRunsAndBalls = new HashMap<>();

        for (Delivery delivery : deliveries) {

            if (matchIdsOf2015.contains(delivery.getMatchId())) {
                String bowlerName = delivery.getBowler();
                int wideRuns = Integer.parseInt(delivery.getWideRuns());
                int noBallRuns = Integer.parseInt(delivery.getNoBallRuns());
                int batsmanRuns = Integer.parseInt(delivery.getBatsmanRuns());

                if (!bowlerRunsAndBalls.containsKey(bowlerName)) {
                    bowlerRunsAndBalls.put(bowlerName, new ArrayList<>(Arrays.asList(0, 0)));
                }
                bowlerRunsAndBalls.get(bowlerName).set(0, bowlerRunsAndBalls.get(bowlerName).get(0) + wideRuns + noBallRuns + batsmanRuns);

                if (wideRuns == 0 && noBallRuns == 0) {
                    bowlerRunsAndBalls.get(bowlerName).set(1, bowlerRunsAndBalls.get(bowlerName).get(1) + 1);
                }
            }
        }
        List<List<String>> bowlerAndEconomy = new ArrayList<>();

        for (Map.Entry<String, List<Integer>> eachBowlerRunsAndBalls : bowlerRunsAndBalls.entrySet()) {
            String bowlerName = eachBowlerRunsAndBalls.getKey();
            int runsConceded = eachBowlerRunsAndBalls.getValue().get(0);
            int ballsBowled = eachBowlerRunsAndBalls.getValue().get(1);

            double overs_bowled = (double) ballsBowled / 6;
            double economy = runsConceded / overs_bowled;

            bowlerAndEconomy.add(Arrays.asList(bowlerName, String.valueOf(economy)));
        }
        Collections.sort(bowlerAndEconomy, new Comparator<List<String>>() {
            @Override
            public int compare(List<String> firstBowlerAndEconomy, List<String> secondBowlerAndEconomy) {
                return Double.valueOf(firstBowlerAndEconomy.get(1)) < Double.valueOf(secondBowlerAndEconomy.get(1)) ? -1 : 1;
            }
        });

        System.out.println("Top 10 Economical bowlers in 2015 IPL Season");
        System.out.println("*******************************************************");

        for (int count = 1; count <= 10; count++) {
            System.out.println("No " + count + " is " + bowlerAndEconomy.get(count - 1).get(0) + " and economy is " + bowlerAndEconomy.get(count - 1).get(1));
        }
        System.out.println("*******************************************************\n");
    }

    public static void findNoOfWinsOfEachTeamWhileBattingFirst(List<Match> matches) {
        Map<String, List<Integer>> battingFirstTeamAndWins = new HashMap<>();

        for (Match match : matches) {
            if (match.getWinner().equals("")) {
                continue;
            }

            if (match.getTossDecision().equals("bat")) {
                if (!battingFirstTeamAndWins.containsKey(match.getTossWinner())) {
                    battingFirstTeamAndWins.put(match.getTossWinner(), Arrays.asList(0, 0));
                }

                battingFirstTeamAndWins.get(match.getTossWinner()).set(1, battingFirstTeamAndWins.get(match.getTossWinner()).get(1) + 1);
                if (match.getTossWinner().equals(match.getWinner())) {
                    int wins = battingFirstTeamAndWins.get(match.getWinner()).get(0);
                    battingFirstTeamAndWins.get(match.getWinner()).set(0, wins + 1);
                }
            } else {
                String battingTeam = match.getTeam1().equals(match.getTossWinner()) ? match.getTeam2() : match.getTeam1();
                if (!battingFirstTeamAndWins.containsKey(battingTeam)) {
                    battingFirstTeamAndWins.put(battingTeam, Arrays.asList(0, 0));
                }

                battingFirstTeamAndWins.get(battingTeam).set(1, battingFirstTeamAndWins.get(battingTeam).get(1) + 1);
                if (battingTeam.equals(match.getWinner())) {
                    int wins = battingFirstTeamAndWins.get(battingTeam).get(0);
                    battingFirstTeamAndWins.get(battingTeam).set(0, wins + 1);
                }
            }
        }
        System.out.println("Number of matches won By each team in IPL while Batting First");
        System.out.println("*******************************************************");

        for (Map.Entry<String, List<Integer>> teamAndWins : battingFirstTeamAndWins.entrySet()) {
            System.out.println(teamAndWins.getKey() + " team won " + teamAndWins.getValue().get(0) + "/" + teamAndWins.getValue().get(1));
        }
        System.out.println("*******************************************************\n");
    }

    public static void findTopRunScoresInEverySeason(List<Match> matches, List<Delivery> deliveries) {
        Map<String, Set<String>> seasonAndMatchIds = new HashMap<>();
        Map<String, Map<String, Integer>> seasonPlayerAndRuns = new HashMap<>();

        for (Match match : matches) {
            String season = match.getSeason();

            if (!seasonAndMatchIds.containsKey(season)) {
                seasonAndMatchIds.put(season, new HashSet<>());
            }
            seasonAndMatchIds.get(season).add(match.getId());

            if (!seasonPlayerAndRuns.containsKey(season)) {
                seasonPlayerAndRuns.put(season, new HashMap<>());
            }
        }
        for (Delivery delivery : deliveries) {
            for (Map.Entry<String, Set<String>> yearAndId : seasonAndMatchIds.entrySet()) {
                if (!yearAndId.getValue().contains(delivery.getMatchId())) {
                    continue;
                }
                int runsScored = seasonPlayerAndRuns.get(yearAndId.getKey()).getOrDefault(delivery.getBatsman(), 0);
                seasonPlayerAndRuns.get(yearAndId.getKey()).put(delivery.getBatsman(), runsScored + Integer.parseInt(delivery.getBatsmanRuns()));
                break;
            }
        }

        Map<String, List<List<String>>> yearPlayerAndRuns = new TreeMap<>();
        for (Map.Entry<String, Map<String, Integer>> yearplayerRuns : seasonPlayerAndRuns.entrySet()) {
            List<List<String>> playerAndRuns = new ArrayList<>();
            for (Map.Entry<String, Integer> playerRuns : yearplayerRuns.getValue().entrySet()) {
                playerAndRuns.add(Arrays.asList(playerRuns.getKey(), String.valueOf(playerRuns.getValue())));
            }
            Collections.sort(playerAndRuns, new Comparator<List<String>>() {
                @Override
                public int compare(List<String> firstPlayer, List<String> secondPlayer) {
                    return Integer.valueOf(firstPlayer.get(1)) > Integer.valueOf(secondPlayer.get(1)) ? -1 : 1;
                }
            });
            yearPlayerAndRuns.put(yearplayerRuns.getKey(), playerAndRuns);
        }

        System.out.println("Top 10 Run scorers in Every Season");
        System.out.println("*******************************************************");
        for (Map.Entry<String, List<List<String>>> playerAndRuns : yearPlayerAndRuns.entrySet()) {
            System.out.println("Top 10 Run scorers in " + playerAndRuns.getKey() + "\n");
            for (int count = 1; count <= 10; count++) {
                System.out.println(count + "." + playerAndRuns.getValue().get(count - 1).get(0) + " scored " + playerAndRuns.getValue().get(count - 1).get(1));
            }
            System.out.println();
        }
        System.out.println("*******************************************************\n");
    }

    public static void findHighestStrikeRateBatsmanForEachTeamIn2016(List<Match> matches, List<Delivery> deliveries) {
        Set<String> matchIdsOf2016 = new HashSet<>();

        for (Match match : matches) {
            if (match.getSeason().equals("2016")) {
                matchIdsOf2016.add(match.getId());
            }
        }
        Map<String, Map<String, List<Integer>>> teamsPlayersRunsAndBalls = new HashMap<>();
        for (Delivery delivery : deliveries) {
            if (!matchIdsOf2016.contains(delivery.getMatchId())) {
                continue;
            }
            if (!teamsPlayersRunsAndBalls.containsKey(delivery.getBattingTeam())) {
                teamsPlayersRunsAndBalls.put(delivery.getBattingTeam(), new HashMap<>());
            }
            if (!teamsPlayersRunsAndBalls.get(delivery.getBattingTeam()).containsKey(delivery.getBatsman())) {
                teamsPlayersRunsAndBalls.get(delivery.getBattingTeam()).put(delivery.getBatsman(), Arrays.asList(0, 0));
            }
            int runs = teamsPlayersRunsAndBalls.get(delivery.getBattingTeam()).get(delivery.getBatsman()).get(0);
            teamsPlayersRunsAndBalls.get(delivery.getBattingTeam()).get(delivery.getBatsman()).set(0, runs + Integer.parseInt(delivery.getBatsmanRuns()));

            if (delivery.getWideRuns().equals("0")) {
                int balls = teamsPlayersRunsAndBalls.get(delivery.getBattingTeam()).get(delivery.getBatsman()).get(1);
                teamsPlayersRunsAndBalls.get(delivery.getBattingTeam()).get(delivery.getBatsman()).set(1, balls + 1);
            }
        }
        Map<String, List<List<String>>> teamBatsmanAndStrikeRate = new HashMap<>();
        for (Map.Entry<String, Map<String, List<Integer>>> entryOfTeamsPlayersRunsAndBalls : teamsPlayersRunsAndBalls.entrySet()) {
            String battingTeam = entryOfTeamsPlayersRunsAndBalls.getKey();

            List<List<String>> playersList = new ArrayList<>();
            for (Map.Entry<String, List<Integer>> entryOfPlayerRunsAndBalls : entryOfTeamsPlayersRunsAndBalls.getValue().entrySet()) {
                String batsman = entryOfPlayerRunsAndBalls.getKey();
                int runs = entryOfPlayerRunsAndBalls.getValue().get(0);
                int ballsFaced = entryOfPlayerRunsAndBalls.getValue().get(1);
                double strikeRate = ((double) runs / ballsFaced) * 100;
                playersList.add(Arrays.asList(batsman, String.valueOf(strikeRate)));
            }
            Collections.sort(playersList, new Comparator<List<String>>() {
                @Override
                public int compare(List<String> firstPlayer, List<String> secondPlayer) {
                    return Double.valueOf(firstPlayer.get(1)) < Double.valueOf(secondPlayer.get(1)) ? 1 : -1;
                }
            });
            teamBatsmanAndStrikeRate.put(battingTeam, playersList);
        }
        System.out.println("Top Strike Rate batsman for each team in the year 2016");
        System.out.println("*******************************************************");

        for (Map.Entry<String, List<List<String>>> entryOfTeamBatsmanAndStrikeRate : teamBatsmanAndStrikeRate.entrySet()) {
            System.out.println(entryOfTeamBatsmanAndStrikeRate.getKey() + " : " + entryOfTeamBatsmanAndStrikeRate.getValue().get(0).get(0) + " strike rate is " + entryOfTeamBatsmanAndStrikeRate.getValue().get(0).get(1));
        }
        System.out.println("*******************************************************\n");
    }

    public static void findHighestRunScorerAgainstSRHPerSeason(List<Match> matches, List<Delivery> deliveries) {
        Map<String, Set<String>> seasonAndMatchIds = new HashMap<>();
        for (Match match : matches) {
            String season = match.getSeason();
            if (match.getTeam1().equals("Sunrisers Hyderabad") || match.getTeam2().equals("Sunrisers Hyderabad")) {
                if (!seasonAndMatchIds.containsKey(season)) {
                    seasonAndMatchIds.put(season, new HashSet<>());
                }
                seasonAndMatchIds.get(season).add(match.getId());
            }
        }
        Map<String, Map<String, Integer>> seasonPlayerAndRuns = new HashMap<>();
        for (Delivery delivery : deliveries) {
            if (!delivery.getBowlingTeam().equals("Sunrisers Hyderabad")) {
                continue;
            }
            for (Map.Entry<String, Set<String>> entryOfseasonAndMatchIds : seasonAndMatchIds.entrySet()) {
                if (!entryOfseasonAndMatchIds.getValue().contains(delivery.getMatchId())) {
                    continue;
                }
                String season = entryOfseasonAndMatchIds.getKey();
                if (!seasonPlayerAndRuns.containsKey(season)) {
                    seasonPlayerAndRuns.put(season, new HashMap<>());
                }
                String batsMan = delivery.getBatsman();
                int runs = seasonPlayerAndRuns.get(season).getOrDefault(batsMan, 0);
                seasonPlayerAndRuns.get(season).put(batsMan, runs + Integer.parseInt(delivery.getBatsmanRuns()));
                break;
            }
        }
        Map<String, List<List<String>>> seasonTopPlayerRuns = new TreeMap<>();
        for (Map.Entry<String, Map<String, Integer>> entryseasonPlayerAndRuns : seasonPlayerAndRuns.entrySet()) {
            List<List<String>> playerAndRuns = new ArrayList<>();
            for (Map.Entry<String, Integer> entryPlayerAndRuns : entryseasonPlayerAndRuns.getValue().entrySet()) {
                String batsman = entryPlayerAndRuns.getKey();
                String runs = String.valueOf(entryPlayerAndRuns.getValue());

                playerAndRuns.add(Arrays.asList(batsman, runs));
            }
            Collections.sort(playerAndRuns, new Comparator<List<String>>() {
                @Override
                public int compare(List<String> firstPlayer, List<String> secondPlayer) {
                    return Integer.valueOf(firstPlayer.get(1)) < Integer.valueOf(secondPlayer.get(1)) ? 1 : -1;
                }
            });
            seasonTopPlayerRuns.put(entryseasonPlayerAndRuns.getKey(), playerAndRuns);
        }
        System.out.println("Highest Run Scorer against Sun Risers Hyderabad in every season");
        System.out.println("*******************************************************");
        for (Map.Entry<String, List<List<String>>> entryOfSeasonTopPlayerAndRuns : seasonTopPlayerRuns.entrySet()) {
            System.out.println("In " + entryOfSeasonTopPlayerAndRuns.getKey() + " " + entryOfSeasonTopPlayerAndRuns.getValue().get(0).get(0) + " scored " + entryOfSeasonTopPlayerAndRuns.getValue().get(0).get(1) + " runs");
        }
        System.out.println("*******************************************************\n");
    }

    public static void findMostNoOfBallsFacedBatsmanPerEachTeamInYear1015(List<Match> matches, List<Delivery> deliveries) {
        Set<String> matchIdsOf2015 = new HashSet<>();

        for (Match match : matches) {
            if (match.getSeason().equals("2015")) {
                matchIdsOf2015.add(match.getId());
            }
        }

        Map<String, Map<String, Integer>> teamBatsmanAndBalls = new HashMap<>();
        for (Delivery delivery : deliveries) {

            if (!matchIdsOf2015.contains(delivery.getMatchId())) {
                continue;
            }
            String battingTeam = delivery.getBattingTeam();
            if (!teamBatsmanAndBalls.containsKey(battingTeam)) {
                teamBatsmanAndBalls.put(battingTeam, new HashMap<>());
            }

            String batsman = delivery.getBatsman();
            if (delivery.getWideRuns().equals("0")) {
                int ballsFaced = teamBatsmanAndBalls.get(battingTeam).getOrDefault(batsman, 0);
                teamBatsmanAndBalls.get(battingTeam).put(batsman, ballsFaced + 1);
            }
        }

        Map<String, List<String>> teamBatsmanAndMostBallsFaced = new TreeMap<>();
        for (Map.Entry<String, Map<String, Integer>> entryOfTeamBatsmanAndBalls : teamBatsmanAndBalls.entrySet()) {

            String battingTeam = entryOfTeamBatsmanAndBalls.getKey();
            String batsman = "";
            int mostballsFaced = 0;
            for (Map.Entry<String, Integer> entryOfBatsmanAndBalls : entryOfTeamBatsmanAndBalls.getValue().entrySet()) {
                if (entryOfBatsmanAndBalls.getValue() > mostballsFaced) {
                    mostballsFaced = entryOfBatsmanAndBalls.getValue();
                    batsman = entryOfBatsmanAndBalls.getKey();
                }
            }
            teamBatsmanAndMostBallsFaced.put(battingTeam, Arrays.asList(batsman, String.valueOf(mostballsFaced)));
        }

        System.out.println("Most balls faced batsman per each team in 2015 \n");
        for (Map.Entry<String, List<String>> entryOfTeamBatsmanAndMostBallsFaced : teamBatsmanAndMostBallsFaced.entrySet()) {
            System.out.println(entryOfTeamBatsmanAndMostBallsFaced.getKey());
            System.out.println("        " + entryOfTeamBatsmanAndMostBallsFaced.getValue().get(0) + "   " + entryOfTeamBatsmanAndMostBallsFaced.getValue().get(1));
        }
    }
}